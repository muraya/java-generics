package com.techpoa;

//We define the type parameter T or any variable you want
// T defines the type of thing the printer is going to print
public class Printer<T> {

    T thingToPrint;

    public Printer(T thingToPrint)
    {
        this.thingToPrint = thingToPrint;
    }

    public  void print()
    {
        System.out.println(thingToPrint);
    }

}
