package com.techpoa;

public class Main {
    public static void main(String[] args) {

        // The integer we are passing here is the one that replaces the type of T in the printer class
        // so in this case an integer
        Printer<Integer> integerPrinter = new Printer<>(23);
        integerPrinter.print();

        Printer<String> stringPrinter = new Printer<>("Hello");
        stringPrinter.print();

        // we have created single class that can print anything that we want

    }
}